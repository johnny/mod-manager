from os.path import expanduser
#################################################
############# Example Configuration #############
#################################################

# use expanduser to extend '~' to the current users home directory e.g. if the
# users name is bob expanduser('~/some/path') -> '/home/bob/some/path', the use
# of expanduser and the '~' notation is completely optional


# *******************************************************
# ---------------EDIT THESE SETTINGS---------------------
# You will probably need to adjust most if not all of the
# following settings.

# location of the arma3server base installation
# Note: If arma isn't already installed updating the server from within
# mod-manager will install it to this location.
# without expanduser:
arma3_server_install = '/home/steam/steamcmd/arma3/install'
# expanduser equivalent:
# arma3_server_install = expanduser('~/steamcmd/arma3/install')

# path to the steamcmd executable
steamcmd_executable = expanduser('~/steamcmd/steamcmd.sh')

# steam workshop content folder.
# This is there steam stores downloaded workshop content
content_path = expanduser('~/Steam/steamapps/workshop/content') 
#********************************************************


# ******************************************************
# ---------------MAYBE EDIT THESE SETTINGS--------------
# If you are very fancy you might want to change these ;).

# folder containing the instance directories
instance_path = expanduser('~/.mod-manager/arma3instances')


# server.cfg template, note: use '{{}}' instead of '{}'
# You can change this to your prefered defaults. The specific config files are
# found in the instance directories and can be safely edited by hand.
server_cfg_template = '''passwordAdmin = "{admin_password}";
password = "{server_password}";
hostname = "{hostname}";
logFile = "{logfile}";
maxPlayers = 20;
motd[] = {{"hello ",
           "...",
           "this is the message of the day..."}};
motdInterval = 0.2;
voteThreshold = 0.5;
voteMissionPlayers = 1;
upnp = 0;
verifySignatures=0; //addon/mod sig verifiction is off
vonCodeQuality = 15;
vonCodec = 1; //use opus codec
allowedVoteCmds[] = {{
{{"missions", true, "true"}},
{{"mission", true, true}},
{{"kick", false, false, 0.75}},
{{"restart", false, true}}, // invalid threshold value. Will default to global "voteThreshold"
{{"reassign", true, true}}
}};
allowedVotedAdminCmds[] = {{}}; //disable all voted admin commands
enableDebugConsole = 1;

 
// SCRIPTING ISSUES
onUserConnected = "";				//
onUserDisconnected = "";			//
doubleIdDetected = "";				//
//regularCheck = "{{}}";				//  Server checks files from time to time by hashing them and comparing the hash to the hash values of the clients. //deprecated
 
// SIGNATURE VERIFICATION
onUnsignedData = "kick (_this select 0)";	// unsigned data detected
onHackedData = "kick (_this select 0)";		// tampering of the signature detected
onDifferentData = "";				// data with a valid signature, but different version than the one present on server detected
  
// MISSIONS CYCLE (see below)
class Missions {{}};				// An empty Missions class means there will be no mission rotation 
missionWhitelist[] = {{}}; //an empty whitelist means there is no restriction on what missions' available
'''
# ******************************************************


# *************************************************
# ------------DON'T EDIT THESE SETTINGS------------
# The following settings are best left alone unless
# you now exactly what you are doing, and BI or
# Valve changed stuff.

# Location where arma3server expects profiles and savegames to be found.
profile_path = expanduser('~/.local/share/Arma 3 - Other Profiles')

# steam id of Arma 3 and the dedicated server
game_id = '107410'
server_id = '233780'

# name of the instance specific directory, containing the enabled mods
# of this instance
mod_dir_name = 'mods'

# steamcmd seems to have a hardcoded timeout of ~300sec for
# downloading workshop items, this sucks for large mods :( but
# retrying with the validate seems to continue where steam stopped
# before.  this number specifies how often we retry downloading after
# steam timed out to increase the effective timeout limit to
# retry_on_timeout*300 seconds
retry_on_timeout = 3
# ************************************************

