"""This module provides tools that are useful for building more
complex/nested menu structures with pythondialog.
It provides the Menu class and the menu_entry decorater factory."""


def menu_entry(title, description):
    """A decorator factory, to decorate menu entry functions with the necessary
    title and description.
    """
    def decorator(obj):
        obj.title = title
        obj.description = description
        return obj
    return decorator


class Menu:
    """A Menu is a structural representation of a selection menu, automating the
    process of evaluating the user's choice and acting upon it and then
    returning to the menu.

    Calling its enter() function, displays the menu and then executes the user's
    chosen entry, after the choice was executed, the menu is displayed again and
    the user can make another choice. The enter() function returns only when the
    user leaves the menu by pressing "esc" or choosing "Cancel".

    A menu entry is a callable with a str attribute title and a str attribute
    description.  The callable shall except as first argument a dialog instance,
    and a config object of abitrary type. The dialog instance can be used by the
    entry to display dialogs etc.. While the config object could be anything,
    its intended purpose is to share some state (like configuration information,
    constants, etc.) between all menu entries without having to rely on global
    variables. A menu entry is called when it is selected by the user.  Menu
    entries are easily created by decorating a callable with the @menu_entry()
    decorater factory.

    The update hook is a user provided function that receives an instance of
    this class (a.k.a. self) as its only argument. The hook is called by an Menu
    instance just before displaying the menu entries, the instance passes itself
    to the hook. Since the hook receives self it can mutate the Menu obect as it
    likes, a hook could e.g. add/remove entries, change the title or
    description, exchange itself for a new hook, etc.  Currently all attributes
    that can be passed to __init__() can be safely modified within an update
    hook.
    """

    def __init__(self, text, entries, title="", description="",
                 update_hook=None, **dialog_args):
        """Creates a menu.

        The names of thes arguments are also the names of the corresponding
        attributes:
        text  -- the text displayed at the top of the menu
        entries -- a list of menu entries
        title -- the name of the menu as it appears in other menu listings
        description -- a short description of this menu
        update_hook -- callable that receives self, called before displaying the
        menu and after every time an entry returns
        dialog_args -- unspecified keyword arguments passed to the Dialog
                  instance used for displaying this menu.
        """

        self.dialog_args = dialog_args
        self.text = text
        self.description = description
        self.title = title
        self.entries = entries
        if not update_hook:
            self.update_hook = lambda x: None
        else:
            self.update_hook = update_hook

    def __call__(self, dialog, config):
        """Wrapper for enter.
        Allows Menu instances to be used as menu entries."""
        self.enter(dialog, config)

    def add_menu_entry(self, entry):
        """Add the entry to this menu.

        Menu entries can be easily created by using the @menu_entry() decorator
        factory.

        entry -- a menu entry a object with a member title, a member description
                 and a __call__ method. title is the displayed name and unique
                 identifier of the menu entry. description is a quick
                 description of the entry, displayed next to the title. __call__
                 shall accept a dialog instance, and a config object to pass
                 arbitrary configuration bindings.
        """
        self.entries.append(entry)

    def enter(self, dialog, config):
        """Display this menu using the provided Dialog instance and execute/carry out
        any of the users choices until they leave the menu by pressing "esc" or
        selecting "Cancel".

        The provided config object is passed on to the menu entries.
        """
        while True:
            self.update_hook(self)

            choices = [(entry.title, entry.description) for entry in self.entries]

            rc, chosen = dialog.menu(text=self.text, choices=choices, **self.dialog_args)

            if rc == dialog.OK:
                # find chosen entry
                entry = next(filter(lambda x: x.title == chosen, self.entries))
                # execute entry
                entry(dialog, config=config)
            elif rc == dialog.ESC or rc == dialog.CANCEL:
                break
            else:
                assert False, 'Call your dev if you read this message.'
