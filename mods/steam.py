"""This module provides a handy wrapper to the SteamCMD
program for downloading workshop content and apps from steam.
"""
import requests
import io
import re


class LoginFailure(Exception):
    pass


class LoginFilter(io.TextIOWrapper):
    """Wrapper for file like objects that filters out lines conatining steam login
    information.
    For text mode files.
    """
    def __init__(self, wrapped):
        super().__init__(wrapped.buffer)
        self.__wrapped = wrapped

    def write(self, string):
        super().write(re.sub(r'(login)\s*.*\s*.*', r'\1 ***** *****', string))

    # FIXME: look into stange behaviour: then no close method is specified or we
    # simply hand through the call to TextIOWrapper, or to the wrapped stdout,
    # stdout gets closed, and we get an Exception when we print our 'Good Bye!'
    # message. But when stdout is passed directly to pexpect it does not get
    # closed..., why?
    #
    # Also interesting: all exceptions raised in the close method, even
    # BaseException, seem to be catched, ... maybe by pexpect?
    def close(self):
        # raise BaseException("blub") # does nothing
        pass


class SteamCMD:
    prompt = '\x1b\[1m\r\nSteam>'
    linesep = '\r\n'

    def __init__(self, executable, args=[], user='anonymous', pwd=None,
                 logfile=None):
        """Creates and initializes a SteamCMD object by logging into steam via the
        provided steamcmd executable.

        Raises a LoginFailure if login was unsuccessfull.
        executable -- the steamcmd executable to be used
        args -- list of arguments passed to SteamCMD (default=[])
        user -- username for login (default='anonymous')
        pwd -- password for login (default=None)
        logfile -- file like object, to log all communications with steamcmd
        """
        import pexpect

        self._proc = pexpect.spawn(executable, args=args, encoding='utf-8')
        if logfile:
            self._proc.logfile_read = LoginFilter(logfile)

        if pwd:
            self._proc.sendline('login "{}" "{}"'.format(user, pwd))
        else:
            self._proc.sendline('login "{}"'.format(user, pwd))

        rc = self._proc.expect(['Login Failure:.*FAILED',
                                'Logged in OK',
                                'Steam Guard code:'])
        if rc == 0:
            raise LoginFailure(self._proc.after[:-len('\nFAILED')])
        elif rc == 2:
            raise NotImplementedError('Steam Guard handling not implemented yet')

    def _sendline(self, line):
        """Block until SteamCMD offers a prompt again, and then send the given line.
        """
        self._proc.expect(self.prompt)
        self._proc.sendline(line)

    def close(self):
        """Stop SteamCMD"""
        self._sendline('quit')
        # FIXME: race condition when sending close right after 'quit'
        # self._proc.close()

    def download_workshop_items(self, app_id, ids, retry_on_timeout=3):
        """Download all the given workshop items via SteamCMD and resolve collections.

        The items are first looked up via the Steam Web API and any collections
        are expanded to their members which are downloaded instead of the
        collection itself.

        Returns a list of the results of download_workshop_item().
        """
        result = []
        for coll in get_collection_details(ids):
            if coll['is_collection']:
                for item in coll['items']:
                    result.append(
                        self.download_workshop_item(app_id, item,
                                                    retry_on_timeout=retry_on_timeout))
            else:
                result.append(
                    self.download_workshop_item(app_id, coll['id'],
                                                retry_on_timeout=retry_on_timeout))
        return result

    def install_update_app(self, app_id, install_dir=None, validate=False):
        """Install or update the given app.

        This is a wrapper for the steamcmd commands app_update and
        force_install_dir.

        Args:
          app_id -- the ID of the app to update/install

          install_dir -- to specify the directory into which the new/updated
                         files will be installed, if None the force_install_dir
                         option will be omitted (default=None)
          validate -- if True validate files after installation (default=False)
        """
        if install_dir:
            self._sendline('force_install_dir "{}"'.format(install_dir))
        if validate:
            validate = 'validate'
        else:
            validate = ''
        self._sendline('app_update "{}" "{}"'.format(app_id, validate))
        return self._check()

    def download_workshop_item(self, app_id, item_id, retry_on_timeout=3):
        """Download the given workshop item.

        This is a wrapper for the steamcmd command of the same name.  Returns a
        tuple of a boolean indicating success and an error/success message.

        Args:
          app_id -- the ID of the main application
          item_id -- the publishedid of the workshop item
        """
        for i in range(0, retry_on_timeout+1):
            if i > 0:
                print('Retrying download of {}. Retry: {}.'.format(item_id, i))
            self._sendline(
                'workshop_download_item "{}" "{}" validate'.format(app_id, item_id))
            succ, msg = self._check()
            if not msg.startswith('ERROR! Timeout downloading item'):
                return succ, msg
        return succ, msg

    def _check(self):
        """Waits for a SUCCESS or ERROR message, and returns a tuple of a boolean
        indicating success and the error/success message.
        """
        rc = self._proc.expect(['Success.*' + self.prompt,
                                'ERROR!.*' + self.prompt,
                                'Error!.*' + self.prompt],
                               timeout=None)
        success = rc == 0
        # get the matched message, remove the matched prompt from the end
        message = self._proc.after[:-len(self.prompt)]
        self._proc.sendline("")  # trigger a new 'un-expected' prompt
        return success, message


def list2steamarray(lst, name):
    """Convert the given list into a dictionary that makes up a Steam Array
    with the given name.

    Example:
      [elem1, elem2, ...], 'somename'
      -> {'somename[0]': elem1, 'somename[1]': elem2, ...}
    lst -- the list of elements
    name -- the name of the array
    """
    array = {}
    for (index, elem) in enumerate(lst):
        array['{}[{}]'.format(name, index)] = elem
    return array


def get_collection_details(publishedids):
    """
    publishedids -- list of publishedids of collections for which details should
    be gotten.
    """
    def parse_collection(coll):
        pid = int(coll['publishedfileid'])
        is_collection = coll['result'] == 1
        if is_collection:
            items = [int(item['publishedfileid']) for item in coll['children']]
        else:
            items = None
        return {'id': pid, 'items': items, 'is_collection': is_collection}

    url = ('https://api.steampowered.com/ISteamRemoteStorage/'
           + 'GetCollectionDetails/v1/')
    data = {'collectioncount': len(publishedids)}
    data.update(list2steamarray(publishedids, 'publishedfileids'))
    resp = requests.post(url, data=data)
    if resp.ok:
        return [parse_collection(coll)
                for coll in resp.json()['response']['collectiondetails']]
    else:
        raise Exception('Received {} from {}'.format(resp.status_code, resp.url))


def get_published_file_details(publishedids, decode_filenames=False):
    """Gets file details for the specified workshop ids.

    publishedids -- list of publishedids of files for which details should
    be gotten.

    decode_filename -- if true, unquotes percent encoded filenames (default=False)

    Note: unnests tags arrays
    """
    def parse_details(details):
        # unnest tags
        details['tags'] = [tag['tag'] for tag in details['tags']]
        #decode filename
        if decode_filenames and details['filename']:
            details['filename'] = requests.utils.unquote(details['filename'])
        return details

    url = ('https://api.steampowered.com/ISteamRemoteStorage/'
           + 'GetPublishedFileDetails/v1/')
    data = list2steamarray(publishedids, 'publishedfileids')
    data['itemcount'] = len(data)
    resp = requests.post(url, data=data)
    if resp.ok:
        return list(map(parse_details,
                        resp.json()['response']['publishedfiledetails']))
    else:
        raise Exception('Received {} from {}'.format(resp.status_code, resp.url))
