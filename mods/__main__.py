from mods.gui import main
import sys
import config

if __name__ == '__main__':
    sys.exit(main(config))
