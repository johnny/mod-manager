"""This module contains the functions related to mod and instance management:
finding installed mods and instances, enabling, disabling mods in instances,
etc.

Supports mods and scenarios downloaded via Steam.
"""
import os
import os.path as p
import pathlib
import glob
import urllib.parse
from mods.steam import get_published_file_details
from mods.util import LoggingMixin, update_symlinks, recursively_lower_case, parse_meta_info


#  Mod loaders #

class ModNotFound(Exception):
    pass


def load_steam_mod(location, details=None):
    meta_file = 'meta.cpp'
    if not p.exists(p.join(location, meta_file)):
        raise ModNotFound("%s is not a steam mod." % location)
    return SteamMod(**parse_meta_info(p.join(location, meta_file)),
                    location=location, file_details=details)


def load_steam_legacy_mod(location, details=None):
    if not p.basename(location).isdigit():
        raise ModNotFound('basename is not digits')
    if len(glob.glob(p.join(location, '*_legacy.bin'))) != 1:
        raise ModNotFound('legacy.bin not found')
    else:
        bin_name = p.basename(glob.glob(p.join(location, '*_legacy.bin'))[0])
    if not details:
        details = get_published_file_details(
            [p.basename(location)], decode_filenames=False)[0]

    __, extension = p.splitext(details['filename'])
    if extension != '.pbo':
        raise ModNotFound('legacy mod is not a pbo file, maybe a collection?')

    if 'Scenario' not in details['tags']:
        raise ModNotFound('legacy mod does not have tag "Scenario"')

    return SteamLegacyMod(details['publishedfileid'],
                          details['title'],
                          location,
                          bin_name,
                          details['filename'],
                          file_details=details)


# FIXME: program often assumes that p.basename(mod.location) is unique
# !! PROBLEM !!
# This module assumes that there will be no filename collisions in the mods and
# mpmissions directories, while the first assumption holds as long as the only
# supported mods are from the steam workshop the second assumption is already
# false.


# uses provided ModLoaders and other "configuration" data to provide a list of
# mod objects
# provides the discover function
class ModDiscovery(LoggingMixin):
    def __init__(self, *searchpaths):
        self.searchpaths = searchpaths
        self.mod_loaders = [load_steam_mod, load_steam_legacy_mod]

    def load(self, path):
        """Tries to load the mod at path by trying the mod_loaders in order.
        Returns the loaded mod or raises ModNotFound."""
        for load_mod in self.mod_loaders:
            try:
                return load_mod(path)
            except ModNotFound as ex:
                self.logger.debug(str(ex))
        raise ModNotFound

    def explore_dir(self, path):
        """Load all mods in the directory path."""
        mod_paths = (p.join(path, name) for name in os.listdir(path))
        mods = []
        for mod_path in mod_paths:
            try:
                mods.append(self.load(mod_path))
            except ModNotFound as ex:
                self.logger.info(str(ex))
        return mods

    def discover(self):
        """Returns a list of mods."""
        mods = []
        for path in self.searchpaths:
            mods.extend(self.explore_dir(path))
        return mods


class SteamWorkshopLoader(LoggingMixin):
    def __init__(self, content_path):
        self.game_id = int(p.basename(content_path))
        self.content_path = content_path
        self.mod_loaders = [load_steam_mod, load_steam_legacy_mod]

    def _load(self, path, details=None):
        for load_mod in self.mod_loaders:
            try:
                return load_mod(path, details)
            except ModNotFound as ex:
                self.logger.debug(str(ex))
        raise ModNotFound('Location {} could not be loaded.'.format(path))

    def discover(self):
        subdirs = os.listdir(self.content_path)
        mod_ids = list(map(int, filter(lambda x: x.isdigit(), subdirs)))

        # get details from steam workshop
        details = {int(mod['publishedfileid']): mod for mod
                   in get_published_file_details(mod_ids)}

        mods = list()
        for mod_id in mod_ids:
            try:
                mods.append(self._load(p.join(self.content_path, str(mod_id)),
                                       details.get(mod_id)))
            except ModNotFound as ex:
                self.logger.info(str(ex))
        return mods


# Mods #

# Base Class
class Mod(LoggingMixin):
    def __init__(self, name, location):
        """Create a mod."""
        self.name = name
        self.location = location

    def keys(self):
        """Find addon signature files.
        Returns a list of paths to .bisign signature files for this mod."""
        return glob.iglob(p.join(self.location, '**/*.bisign'))

    def __repr__(self):
        """Return string representation."""
        return self.__class__.__name__ + '(%s)' % repr(self.__dict__)

    def __eq__(self, other):
        return type(self) == type(other) and self.__dict__ == other.__dict__

    def __hash__(self):
        return hash(self.location)

    def ctime(self):
        return os.stat(self.location).st_ctime

    def enabled_in(self, instance):
        """Returns wether this mod is enabled in the given instance."""
        path = p.join(instance.location, instance.mod_dir,
                      p.basename(self.location))
        if p.exists(path) and p.islink(path):
            if os.readlink(path) == self.location:
                return True
            else:
                self.logger.warn('Potential mod name collision!')
                return False
        else:
            return False

    def add_to(self, instance):
        """Enable this mod in the given instance.

        Create a symlink from instance/mod_dir/<id of mod> to the location of
        mod.
        """
        # FIXME: Assumption that basename(mod.location) is unique
        mod_dst = p.join(instance.location, instance.mod_dir,
                         p.basename(self.location))
        key_dst = p.join(instance.location, 'keys')

        if not p.exists(mod_dst) and p.exists(self.location):
            os.symlink(self.location, mod_dst, target_is_directory=True)
            for key in self.keys():
                os.symlink(key, p.join(key_dst, p.basename(key)))

        elif p.exists(self.location):
            self.logger.warn(
                'Not enabling mod ' + self.name + ': already enabled.')
        else:
            self.logger.warn(
                'Not enabling mod ' + self.name + ': mod not found.')

    def remove_from(self, instance):
        """Disable this mod in the given instance.

        Remove the symlink instance/mod_dir/<id of mod>.
        """
        # FIXME: Assumption that basename(mod.location) is unique
        mod_link = p.join(instance.location, instance.mod_dir,
                          p.basename(self.location))
        keys = p.join(instance.location, 'keys')
        if p.exists(mod_link):
            # remove signatures
            for key in (p.join(keys, p.basename(key)) for key in self.keys()):
                if p.exists(key):
                    os.remove(key)
                else:
                    self.logger.warn(
                        'disable_mod: ' + self.name + ' missing keyfile '
                        + key + ': Skipping.')
            os.remove(mod_link)

        elif p.exists(self.location):
            self.logger.warn(
                'Not disabling mod ' + self.name + ': already disabled.')
        else:
            self.logger.warn(
                'Not disbaling mod ' + self.name
                + ': no such mod, possibly a typo?')


class SteamMod(Mod):
    def __init__(self, publishedid, name, location, timestamp=None,
                 file_details=None, **kwargs):
        """Create a steam workshop mod."""
        super().__init__(name, location)
        self.publishedid = publishedid
        self.timestamp = timestamp

        # /steam/.../<game_id>/<published_id>
        self.game_id = p.basename(p.dirname(location))
        assert self.game_id.isdigit()

        self._cached_file_details = file_details
        if kwargs:
            self.logger.debug('Received additional kwargs: %s', kwargs)

    def __str__(self):
        return '[Mod, {}]{}'.format(self.publishedid, self.name)

    @property
    def _file_details(self):
        if not self._cached_file_details:
            self._cached_file_details = get_published_file_details(
                [self.publishedid])[0]
        return self._cached_file_details

    def out_of_date(self):
        """Returns wether the mod is out of date or not.

        details -- file details as provided by steam.get_published_file_details,
        when provided use the cached value instead of making a new request.
        """
        return self.ctime() < self._file_details['time_updated']

    def update(self, steam):
        """Update this mod via the workshop.

        steam -- (logged in) SteamCMD instance
        """
        # TODO: instead of touching location on update, determine ctime smarter
        # Maybe: walk dir tree and use max ctime, or
        # use locations known to change (maybe meta.cpp, legacy.bin for LegacyMod)
        pathlib.Path(self.location).touch()  # update ctime of mod location
        ok, msg = steam.download_workshop_item(self.game_id, self.publishedid)
        recursively_lower_case(self.location)
        return ok, msg


# class LocalMod:
#     pass


# Scenario/Mission?
class SteamLegacyMod(SteamMod):
    """Certain mods are downloaded by steam solely as XXXX_legacy.bin files a web
    api request is necessary to find out what these files actually are and then
    link them accordingly.
    """
    def __init__(self, publishedid, name, location, bin_name, filename,
                 file_details=None):
        # retrieve info from web?
        super().__init__(publishedid, name, location, file_details=file_details)
        self.filename = filename
        self.bin_name = bin_name

    def __str__(self):
        return '[Scenario, {}]{}'.format(self.publishedid, self.name)

    def enabled_in(self, instance):
        """Returns wether this mod is enabled in the given instance."""
        path = p.join(instance.location, 'mpmissions', self.filename)
        if p.exists(path) and p.islink(path):
            if os.readlink(path) == p.join(self.location, self.bin_name):
                return True
            else:
                self.logger.warn('Potential mod name collision!')
                return False
        else:
            return False

    def add_to(self, instance):
        """Enable this mod in the given instance.

        Create a symlink from instance/mpmission/<filename of mod> to the
        location of mod.
        """
        # FIXME: Assumption that self.filename is unique
        mod_dst = p.join(instance.location, 'mpmissions', self.filename)
        mod_src = p.join(self.location, self.bin_name)

        if not p.exists(mod_dst) and p.exists(mod_src):
            os.symlink(mod_src, mod_dst)
        elif p.exists(mod_src):
            self.logger.warn('Not enabling mod ' + self.name
                             + ': already enabled.')
        else:
            self.logger.warn('Not enabling mod ' + self.name
                             + ': mod not found.')

    def remove_from(self, instance):
        """ Disable this mod in the given instance.

        Remove the symlink instance/mpmission/<filename of mod>.
        """
        # FIXME: Assumption that self.filename is unique
        mod_link = p.join(instance.location, 'mpmissions', self.filename)

        if p.exists(mod_link):
            os.remove(mod_link)
        elif p.exists(p.join(self.location, self.bin_name)):
            self.logger.warn(
                'Not disabling mod ' + self.name + ': already disabled.')
        else:
            self.logger.warn('Not disbaling mod ' + self.name
                             + ': no such mod, possibly a typo?')


# Instance #

# name of the instance specific directory, containing the enabled mods
# of this instance
mod_dir_name = 'mods'


def new_instance(base_dir, name, admin_passw, passw, server_cfg_template,
                 arma3_install,
                 hostname='Arma 3 Server',
                 log_file='server.log',
                 mod_dir=mod_dir_name,
                 config='server.cfg'):
    text = server_cfg_template.format(admin_password=admin_passw,
                                      server_password=passw,
                                      hostname=hostname,
                                      logfile=p.join(base_dir, name, log_file))
    # create instance directory
    os.mkdir(p.join(base_dir, name))
    try:
        # create mod directory
        os.mkdir(p.join(base_dir, name, mod_dir))
        # create keys directory
        os.mkdir(p.join(base_dir, name, 'keys'))
        # create mission directory
        os.mkdir(p.join(base_dir, name, 'mpmissions'))

        update_symlinks(p.join(base_dir, name), arma3_install)

        # create server config file
        config = p.join(base_dir, name, config)
        with open(config, 'w') as f:
            f.write(text)
    except Exception:
        # clean up in case of exception
        os.remove(p.join(base_dir, name))
        raise


def dumb_discover_instances(path):
    # dumb in the sense that it only uses/sets instance location and does not
    # detect config name, mod dir name, etc.
    instances = os.listdir(path)
    return [Instance(p.join(path, name)) for name in instances]


class Instance:
    def __init__(self, location,
                 config='server.cfg', mod_dir=mod_dir_name,
                 world='empty', options=''):
        self.name = p.basename(location)
        self.location = location
        self.config = config
        self.mod_dir = mod_dir
        self.world = world
        self.options = options

    def update(self, arma3_install):
        """Update symlinks from the instance to the Arma 3 install."""
        update_symlinks(self.location, arma3_install)

    def start(self, executable='./arma3server'):
        """Start this instance.

        executable -- path to the Arma 3 executable to run, relative to this
        instance's location (default='./arma3server')
        """
        cmd = '"{}" -config="{}" -name="{}" -mod="{}" -world="{}" {}'

        # Arma 3 does not handle whitespaces in the -name parameter correctly
        # (names get cut off at the whitespace) thus we percent-encode the
        # instance name before passing it to Arma.
        # all reserved characters are safe since we only care about spaces
        safe = ';/?:@&=+$,'
        quoted_name = urllib.parse.quote(self.name, safe=safe)

        # collect all mods enabled in this instance
        mods = ''
        for mod in os.listdir(p.join(self.location, self.mod_dir)):
            mods += '{};'.format(p.join(self.mod_dir, mod))

        cmd = cmd.format(executable,
                         self.config,
                         quoted_name,
                         mods,
                         self.world,
                         self.options)
        print(cmd)
        cwd = os.getcwd()  # save current working directory
        # change working dir to the instance directory
        os.chdir(self.location)
        os.system(cmd)  # execute command
        os.chdir(cwd)  # restore former working directory

    def enable(self, mod):
        mod.add_to(self)

    def disable(self, mod):
        mod.remove_from(self)
