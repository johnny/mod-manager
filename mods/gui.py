"""This module contains all the menus, the main function, and other gui
stuff."""
from contextlib import closing
import os
import os.path as p
from dialog import Dialog
import logging
from mods.steam import SteamCMD, LoginFailure
from mods.menus import Menu, menu_entry
import mods.modlib as modlib


def lookup(cache, key, fallback):
    """Looks up key in cache.
    If key is not cached call fallback to retrieve the value, cache and return
    it.
    key -- the key to lookup
    cache -- the mapping in which the lookup is performed
    fallback -- a callable accepting no arguments
    """
    if key not in cache:
        cache[key] = fallback()
    return cache[key]


class Canceled(Exception):
    pass


def clear_screen():
    os.system('clear')


def esc_or_cancel(code):
    return code == Dialog.CANCEL or code == Dialog.ESC


def is_ok(code):
    return Dialog.OK == code


@menu_entry('Install', 'download new mods from the steam workshop')
def install_mods(diag, config):
    class BadFormat(Exception):
        pass

    def enter_mods(text):
        return diag.editbox_str(text,
                                title='''Enter workshop IDs of mods to install.
                                Enter one ID per line.''')

    def text_to_mod_ids(text):
        return [int(line.strip()) for line in text.splitlines()
                if line.strip() is not '']

    text = ''
    while True:
        rc, text = enter_mods(text)
        if esc_or_cancel(rc):
            return
        elif is_ok(rc):
            try:
                mods = text_to_mod_ids(text)
                break
            except ValueError as ve:
                diag.msgbox('Error parsing line: ' + str(ve))
    try:
        with closing(login_menu(diag, config)) as steam:  # raises Canceled
            results = steam.download_workshop_items(config.game_id,
                                                    mods,
                                                    config.retry_on_timeout)
            errors = [result[1] for result in results if not result[0]]
            if errors:
                # remove " in errors, since in the Debian version of python-dialog
                # they don't get properly escaped...
                diag.msgbox('The following errors occured:\n'
                            + ('\n'.join(errors)).replace('"', ''))
                clear_screen()
    except Canceled:
        pass


def new_mod(diag):
    # check for active connection, otherwise prompt for login
    raise NotImplementedError


@menu_entry('Update Mods', 'bring installed mods up to date')
def update_mods(dialog, config):
    mod_discovery = modlib.SteamWorkshopLoader(p.join(config.content_path,
                                                      config.game_id))
    mods = mod_discovery.discover()
    config.cache['loaded_mods'] = mods
    try:
        with closing(login_menu(dialog, config)) as steam:
            for mod in mods:
                if mod.out_of_date():
                    ok, msg = mod.update(steam)
                    if not ok:
                        dialog.msgbox(msg)
                        clear_screen()
                else:
                    logging.info("Update: Skipping {} mod allready up to date."
                                 .format(mod))    
    except Canceled:
        return


@menu_entry('Update Arma', 'bring the Arma 3 server up to date')
def update_server(dialog, config):
    try:
        with closing(login_menu(dialog, config)) as steam:
            ok, msg = steam.install_update_app(
                config.server_id, install_dir=config.arma3_server_install,
                validate=True)
    except Canceled:
        return
    dialog.msgbox(msg)

    # update instances
    ipath = config.instance_path
    for instance in modlib.dumb_discover_instances(ipath):
        instance.update(config.arma3_server_install)


def login_menu(diag, config, **kwargs):
    """Prompt the user for a username and password, and return a corresponding
    SteamCMD instance.

    Notable exceptions:
    Canceled() -- raised if the user cancels the login process.

    Args:
    diag   -- the Dialog instance used for the user interaction
    config -- a global configuration
    kwargs -- arbitrary keyword arguments that should be passed to the SteamCMD
              instance
    """
    import sys
    while True:
        rc, username = diag.inputbox('Enter Steam user name:', init='')
        if esc_or_cancel(rc):
            raise Canceled()
        rc, pwd = diag.passwordbox('Enter password for {}:'.format(username),
                                   init='',
                                   insecure=True)
        if esc_or_cancel(rc):
            raise Canceled()
        try:
            clear_screen()
            return SteamCMD(config.steamcmd_executable, user=username, pwd=pwd,
                            logfile=sys.stdout,
                            **kwargs)
        except LoginFailure as lf:
            diag.msgbox(str(lf))


def manage_instance_menu(diag, config, instance):
    """Show the mod selection for the given instance using the given Dialog
    instance.
    """

    mod_discovery = modlib.SteamWorkshopLoader(p.join(config.content_path,
                                               config.game_id))
    available_mods = lookup(config.cache, 'loaded_mods',
                            lambda: mod_discovery.discover())

    available_mods.sort(key=lambda x: x.name.lower())
    # TODO: Let instance provide list of enabled mods?
    choices = [(str(i), str(mod), mod.enabled_in(instance))
               for i, mod in enumerate(available_mods)]

    # modselection checklist
    rc, selected = diag.checklist(text='Enable/Disable mods for '+instance.name,
                                  choices=choices)
    if esc_or_cancel(rc):
        return
    if is_ok(rc):
        selected_mods = {available_mods[int(i)] for i in selected}
        enabled_mods = {mod for mod in available_mods
                        if mod.enabled_in(instance)}
        # disable mods that aren't selected any more
        for mod in enabled_mods.difference(selected_mods):
            mod.remove_from(instance)

        # enable newly selected mods
        for mod in selected_mods.difference(enabled_mods):
            mod.add_to(instance)


@menu_entry('Create new Instance', '')
def new_instance_menu(diag, config=None):
    diag.msgbox(text='You will be prompted to enter a name, '
                + 'a hostname, a logfile location, '
                + 'an admin password and a server password.')
    rc, name = diag.inputbox('Enter a name for the new instance: ', init='')
    if esc_or_cancel(rc): return
    rc, hostname = diag.inputbox('Enter a Hostname that will be '
                                 + 'displayed in the server browser: ',
                                 init='')
    if esc_or_cancel(rc): return
    rc, logfile = diag.inputbox('Enter a the name of the logfile: ', init='server.log')
    if esc_or_cancel(rc): return
    rc, admin_passw = diag.passwordbox('Enter a admin password: ',
                                       init='',
                                       insecure=True)
    if esc_or_cancel(rc): return
    rc, server_passw = diag.passwordbox('Enter the server password, '
                                        + 'preventing random people from '
                                        + 'joining your server:''',
                                        init='', insecure=True)
    if esc_or_cancel(rc): return
    elif is_ok(rc):
        modlib.new_instance(config.instance_path, name,
                            admin_passw, server_passw,
                            config.server_cfg_template,
                            config.arma3_server_install,
                            hostname=hostname,
                            mod_dir=config.mod_dir_name,
                            log_file=logfile)
        diag.msgbox(text='Created new instance ' + name + '.')
    else:
        assert(False)  # should not be reached


# TODO: Mod menu: delete mods
# Idea: programbox for live progress during installation/update
# alternative to new mod: install mods with editbox
mods_menu = Menu(text='Manage mods',
                 title='Mods',
                 description='add new and manage already installed mods',
                 entries=[install_mods,
                          update_mods,
                          update_server])


@menu_entry('Instances', 'create new or modify existing instances')
def instances_menu(diag, config):
    def update(menu):
        # Build a 'manage instance XYZ' entry for all discovered instances.
        # Note that we bind the instance in the lambda as a default argument
        # since default args get evaluated at definition time, while variables
        # in lambda bodies are late binding.
        menu.entries = [
            menu_entry(instance.name, '')(
                lambda diag, config, i=instance: manage_instance_menu(diag, config, i))
            for instance in modlib.dumb_discover_instances(config.instance_path)
        ]
        menu.entries.append(new_instance_menu)

    menu = Menu(text='Select an instance to modify', entries=None, update_hook=update)
    menu.enter(diag, config)


@menu_entry('Start Instance', '')
def start_instance_menu(diag, config):
    instances = {x.name: x
                 for x in modlib.dumb_discover_instances(config.instance_path)}
    choices = [(name, '') for name in instances]
    rc, selected = diag.menu(text='Select an instance to run', choices=choices)
    if esc_or_cancel(rc):
        return
    if is_ok(rc):
        clear_screen()
        with closing(SteamCMD(config.steamcmd_executable)):
            instances[selected].start()
        print('Press enter to continue.')
        input()


main_menu = Menu('Main Menu', cancel_label='Exit',
                 entries=[mods_menu,
                          instances_menu,
                          start_instance_menu])


def main(config):
    diag = Dialog(autowidgetsize=True)
    diag.set_background_title('Arma 3 server management')

    # create missing directories
    for path in (config.instance_path, config.profile_path):
        if not p.exists(path):
            os.makedirs(path)
            logging.warn('Creating missing directory "{}"'.format(path))
        elif not p.isdir(path):
            raise Exception('Path "{}" is not a directory.'.format(path))

    config.cache = dict()
    try:
        main_menu.enter(diag, config)
    except Exception:
        # don't leave a mess on the screen when something went wrong
        clear_screen()
        raise

    clear_screen()
    print('Good Bye!')
    return 0
