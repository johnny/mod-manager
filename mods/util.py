import logging
import os
import os.path as p

class LoggingMixin:
    @property
    def logger(self):
        return logging.getLogger(self.__class__.__name__)


def update_symlinks(destination, source):
    """Remove broken and create missing links from the destination dir to the
    files in source dir.

    Args:
    destination -- path to the directory where symlinks will be created/updated.
    source -- path to the directory to whose contents the links will point.
    """
    destination_dir = list(os.scandir(destination))

    # remove broken symlinks
    for entry in destination_dir:
        if entry.is_symlink() and not p.exists(entry.path):
            os.remove(entry.path)

    # add missing links
    for entry in os.listdir(source):
        if entry not in [x.name for x in destination_dir]:
            os.symlink(p.join(source, entry), p.join(destination, entry))


def recursively_lower_case(path, depth=7):
    """Recursively convert the name of the given file or directory and its
    subdirectories to lower case.

    keyword arguments:
      depth -- the maximum recursion depth (default: 7)
    """
    # make filename lowercase
    new_path = p.join(p.dirname(path), p.basename(path).lower())
    os.rename(path, new_path)

    # descent into subdirectory
    if depth > 0 and p.isdir(new_path):
        for child in os.scandir(new_path):
                recursively_lower_case(child.path, depth-1)


def parse_meta_info(path):
    """Parse Steam/Arma 3 meta.cpp files."""
    import re

    def parse(line):
        regex_str = r'\s*([A-Za-z]\w*)\s*=\s*"([^"]*)"\s*;\s*'
        regex_int = r'\s*([A-Za-z]\w*)\s*=\s*(\d+)\s*;\s*'
        match_str = re.match(regex_str, line)
        match_int = re.match(regex_int, line)
        if match_str:
            return match_str.group(1), match_str.group(2)
        elif match_int:
            return match_int.group(1), int(match_int.group(2))
        else:
            raise(Exception('Unmachted line: ' + line + '\nIn file: ' + path))

    meta = {}
    with open(path) as f:
        for line in f:
            if line is '':
                continue
            key, val = parse(line)
            meta[key] = val
    return meta
